# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 14:51:39 2020

@author: rodri
"""

"""
------------------------------------------------------------------------------
                                Import Packages
------------------------------------------------------------------------------
"""

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import tkinter as tk
import numpy as np
import serial as sr
import time
from scipy.signal import find_peaks
import pandas as pd
import time 


"""
------------------------------------------------------------------------------
                                 Variables
------------------------------------------------------------------------------
"""
all_peak_data = []

"""
------------------------------------------------------------------------------
                               Storing Data
------------------------------------------------------------------------------
"""
# Give a name to the file
time = time.localtime()
fileName = str('Data/Workout' +       \
             "_" + str(time[0]) +      \
             "_"  + str(time[1]) +     \
             "_"  + str(time[2]) +     \
             "_"  + str(time[3]) +     \
             "_" + str(time[4])   +    \
             ".csv")
    
# Create txt file to read the latest workout
file_workout= open("latest_workout.txt", "w")
file_workout.write(fileName)
file_workout.close()

"""
------------------------------------------------------------------------------
                                    Global
------------------------------------------------------------------------------
"""
data = np.array([])
cond = False


"""
------------------------------------------------------------------------------
                                  PlottingData
------------------------------------------------------------------------------
"""
def plot_data():
    # Condition for Buttons
    global cond, data, all_peak_data, peaks
    
    if cond == True: # if the condition is true it will read the data
        s.readline()
        for i in range(20): 
            s.readline()
        a = s.readline()
        a.decode()
        
        if (len(data) <100): # we only want the 100 values of the data, this is how we shift
            data = np.append(data, float(a[0:4]))
            all_peak_data.append(data[-1])
        else:
            data[0:99] = data[1:100]
            data[99] = float(a[0:4])
            all_peak_data.append(data[-1])
            
        peaks, _ = find_peaks(all_peak_data, height=-1)
        ax_2.bar(len(peaks),1)
        lines.set_xdata(np.arange(0, len(data)))
        lines.set_ydata(data)
        
        canvas.draw()
        laps = tk.Label(root, text = str(len(peaks)), font = ('calibri', 161))
        laps.place(x = 525, y = 11)
        
        if len(peaks)>=10 and len(peaks)<15:
            inspirational_text = tk.Label(root, text = "    Swole    ", font = ('calibri',40))
            inspirational_text.place(x = 525, y = 415)
        elif len(peaks)>=15 and len(peaks)<20:
            inspirational_text = tk.Label(root, text = "   Massive   ", font = ('calibri',40))
            inspirational_text.place(x = 525, y = 410)
        elif len(peaks)>= 20:
            inspirational_text = tk.Label(root, text = "Shredded   ", font = ('calibri',40))
            inspirational_text.place(x = 525, y = 410)
            
    root.after(1,plot_data)

"""
------------------------------------------------------------------------------
                              ButtonSartStop
-----------------------------------------------------------------------------
"""
def plot_start():
    global cond
    cond = True
    s.reset_input_buffer()
    
def plot_stop():
    global cond
    cond = False

def plot_end():
    global cond
    cond = False
    s.close()
    data_frame = pd.DataFrame()
    data_frame['Data'] = pd.Series(data)
    data_frame['Strokes'] = pd.Series(len(peaks))
    data_frame['Calories'] = pd.Series(5)
    data_frame['Time'] = pd.Series(5)
    data_frame.to_csv(fileName, index=False)
    
    


"""
------------------------------------------------------------------------------
                               Main GUI
------------------------------------------------------------------------------
"""
root = tk.Tk()
root.title("RealTime Plotting")
root.configure(background = 'light blue') # background
root.geometry("850x500")                  # window size

"""
------------------------------------------------------------------------------
                                Create Figure
------------------------------------------------------------------------------                               
"""

"""
Create Figure Object
"""
fig = Figure()
ax = fig.add_subplot(211)
ax_2 = fig.add_subplot(212)

"""
Set Title
"""
#ax = plt.axes(xlim=(a,100),ylim=((0,120));# displaying only 100 samples
ax.set_title('SerialData')
ax.set_xlabel('Time')
ax.set_label('Acceleration')
ax.set_xlim(0,100)
ax.set_ylim(-1.5,.0)
lines = ax.plot([],[])[0] # want to change the lines within that plot because live
#dots = ax_2.bar([],[])


"""
Canvas
"""
canvas = FigureCanvasTkAgg(fig, master = root)
canvas.get_tk_widget().place(x = 10, y = 10, width = 500, height = 400) # create the canvas
canvas.draw()



"""
------------------------------------------------------------------------------
                                 Create Buttons
------------------------------------------------------------------------------
"""
root.update();
start = tk.Button(root, text = "Start", font = ('calibri', 12), command = lambda: plot_start())
start.place(x = 100, y = 425)

root.update();
stop = tk.Button(root, text = "Pause", font = ('calibri', 12), command = lambda: plot_stop())
stop.place(x = start.winfo_x() +start.winfo_reqwidth() + 20, y = 425)

root.update()
end = tk.Button(root, text = "End", font = ('calibri', 12), command = lambda: plot_end())
end.place(x = stop.winfo_x() + stop.winfo_reqwidth() + 20, y = 425)


"""
------------------------------------------------------------------------------
                                     Organization
------------------------------------------------------------------------------
"""
# Organize where the laps are going to go
# Organize where the Distance is going to go, Could create a button for meters
# or feet whichever works



"""
------------------------------------------------------------------------------
                                  Serial Port
 ----------------------------------------------------------------------------
"""
s = sr.Serial('COM4', 9600)  
s.reset_input_buffer()
                 
root.after(1, plot_data)
root.mainloop()

