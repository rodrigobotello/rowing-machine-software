# -*- coding: utf-8 -*-
"""
Created on Sun Nov 15 21:51:39 2020

@author: pasch
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
#from sklearn.linear_model import LinearRegression 
#from scipy.fft import fft

plt.close('all')

# Find the txt file for the naming
file_workout = open("latest_workout.txt", "r")
filename = file_workout.readline()
data = pd.read_csv(filename)
file_workout.close()

# Set the arrays for the data
time = np.array(data.Data)
y = np.array(data.Data) # [counts]


#---------------------------
# Convert raw data: counts to gs
#---------------------------
conv = 1/1024  # conversion rate from raw data [counts] to units of [g]
y_g = y*conv   # [g] 


N=1000
it1 = 0
it2 = it1 + N

time = time[it1:it2]
y = y[it1 :it2]




#---------------------------
# Cut data at beginning and end of dynamic oscillation:
#---------------------------

T1, T2 = 0, time[-1]
it1 = np.nonzero(time > T1)[0][0]
it2 = np.nonzero(time < T2)[0][-1]

#Tz, Az = time[it1:it2], z_g[it1:it2]

Ty, Ay = time[it1:it2], y_g[it1:it2]
#print(Ty, Ay)

#---------------------------
# Assess Y axis acceleration
# Find the Peaks and Calculate Avg Period
# Find a value of dist that allows for better peak accuracy
#---------------------------
dist = 100

Ay_its, _ = find_peaks(Ay, height=-.01, distance = dist)
print(_)
Ay_peaks  = Ay[Ay_its] # Solves for peak values using wx_its iteration values
Ty_peaks  = Ty[Ay_its]
Ty_avg    = 1
wd_exp    = 0

print(Ay_peaks)
print(Ty_peaks)




# #code for J project
# plt.figure()
# #plt.plot(time, x, label='x')
# plt.plot(time, y, label='y')
# #plt.plot(time, z, label='z')
# plt.grid()
# plt.legend()

#graph that shows peaks
plt.figure()
plt.plot(Ty,Ay)
plt.plot(Ty_peaks,Ay_peaks,'rx')
plt.title('Y acceleration vs Time')
plt.legend(['Yacc','Peaks'])
plt.xlabel('Time [s]')
plt.ylabel('Amplitude [g]')
plt.grid()
plt.show()
