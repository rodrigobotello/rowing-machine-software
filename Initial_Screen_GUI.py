# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 20:01:08 2020

@author: rodri
"""

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import tkinter as tk
import numpy as np
import serial as sr
import time
from scipy.signal import find_peaks
import pandas as pd
import time 
from tkinter import *
from PIL import Image, ImageTk
import os
"""
==============================================================================
                               Function to Convert Time
==============================================================================
"""
def convert(seconds): 
    seconds = seconds % (24 * 3600) 
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
    return "%d:%02d:%02d" % (hour, minutes, seconds) 

"""
==============================================================================
                            Global Variables
==============================================================================
"""
s = sr.Serial('COM4', 9600)  
s.reset_input_buffer()
data = np.array([])
cond = False
all_peak_data = []
"""
==============================================================================
                            Storing Data
==============================================================================
"""
# Give a name to the file
time = time.localtime()
fileName = str('Data/Workout' +       \
             "_" + str(time[0]) +      \
             "_"  + str(time[1]) +     \
             "_"  + str(time[2]) +     \
             "_"  + str(time[3]) +     \
             "_" + str(time[4])   +    \
             ".csv")
    
# Create txt file to read the latest workout
file_workout= open("latest_workout.txt", "w")
file_workout.write(fileName)
file_workout.close()
'''
===============================================================================
                            Button Functionality
===============================================================================
'''
def create_workout_window():
    global root
    """
    -------------------------------------------------------------------------------
                                    Hide Main Menu
    -----------------------------------------------------------------------------
    """
    root.withdraw()
    """
    ------------------------------------------------------------------------------
                                      PlottingData
    ------------------------------------------------------------------------------
    """
    def plot_data():
        # Condition for Buttons
        global cond, data, all_peak_data, peaks, s, start
        
        if cond == True: # if the condition is true it will read the data
            s.readline()
            for i in range(20): 
                s.readline()
            a = s.readline()
            a.decode()
            
            if (len(data) <100): # we only want the 100 values of the data, this is how we shift
                data = np.append(data, float(a[0:4]))
                all_peak_data.append(data[-1])
            else:
                data[0:99] = data[1:100]
                data[99] = float(a[0:4])
                all_peak_data.append(data[-1])
                
            peaks, _ = find_peaks(all_peak_data, height=-.6)
            ax_2.bar(len(peaks),1)
            lines.set_xdata(np.arange(0, len(data)))
            lines.set_ydata(data)
            
            import time
            #total_time = convert(time.time() - start))
            laps_text = tk.Label(new_window, text = "Strokes ", font = ('calibri',25))
            laps_text.place(x = 525, y = 10)
            
            laps_text = tk.Label(new_window, text = "Time ", font = ('calibri',25))
            laps_text.place(x = 730, y = 10)
            
            laps_text = tk.Label(new_window, text = "Distance ", font = ('calibri',25))
            laps_text.place(x = 1300, y = 10)
            
            canvas.draw()
            laps = tk.Label(new_window, text = str(len(peaks)), font = ('calibri', 85))
            laps.place(x = 520, y = 100)
            
            time_elapsed = tk.Label(new_window, text = str(convert(time.time() - start)), font = ('calibri', 85))
            time_elapsed.place(x = 730, y = 100)
            
            distance = tk.Label(new_window, text = str(len(peaks)*6) + '  ft', font = ('calibri',85))
            distance.place(x = 1300, y = 100)
         
            #if len(peaks)>=10 and len(peaks)<15:
             #   inspirational_text = tk.Label(new_window, text = "  Swole ", font = ('calibri',30))
              #  inspirational_text.place(x = 525, y = 350)
            #elif len(peaks)>=15 and len(peaks)<20:
             #   inspirational_text = tk.Label(new_window, text = " Massive ", font = ('calibri',30))
              #  inspirational_text.place(x = 525, y = 350)
            #elif len(peaks)>= 20:
               # inspirational_text = tk.Label(new_window, text = " Shredded ", font = ('calibri',30))
                #inspirational_text.place(x = 525, y = 350)   
        new_window.after(1,plot_data)
    """
    ------------------------------------------------------------------------------
                                  ButtonSartStop
    -----------------------------------------------------------------------------
    """
    def plot_start():
        global cond, s, start
        cond = True
        # start the timer
        import time
        start = time.time()
        s.reset_input_buffer()
        
    def plot_stop():
        global cond, s
        cond = False
    
    def plot_end():
        global cond, s, peaks, start
        cond = False
        s.close()
        root.deiconify()
        new_window.destroy()
        data_frame = pd.DataFrame()
        data_frame['Data'] = pd.Series(data)
        data_frame['Strokes'] = pd.Series(len(peaks))
        #data_frame['Calories'] = pd.Series(5)
        import time
        data_frame['Time'] = pd.Series(convert(time.time() - start))
        data_frame['Distance'] = pd.Series(str(len(peaks)*6) + '  ft')
        data_frame.to_csv(fileName, index=False)
    """
    ------------------------------------------------------------------------------
                                    GUI
    ------------------------------------------------------------------------------
    """
    new_window = tk.Toplevel(root)
    new_window.title("RealTime Plotting")
    image_2 = Image.open("Images/image_4.jpg").resize((1750,1000), Image.ANTIALIAS)
    photo_2 = ImageTk.PhotoImage(image_2)
    canvas_image = tk.Canvas(new_window, width=1750, height=1000)
    canvas_image.pack()
    canvas_image.background = photo_2
    bg = canvas_image.create_image(0,0, anchor=tk.NW, image = photo_2)
    #new_window.configure(background = "light blue") # background
    new_window.geometry("1750x750")                  # window size
    """
    ------------------------------------------------------------------------------
                                    Create Figure
    ------------------------------------------------------------------------------                               
    """
    """
    Create Figure Object
    """
    fig = Figure()
    ax = fig.add_subplot(211)
    ax_2 = fig.add_subplot(212)
    """
    Set Title
    """
    ax.set_title('SerialData')
    ax.set_xlabel('Time')
    ax.set_label('Acceleration')
    ax.set_xlim(0,100)
    ax.set_ylim(-1.5,.0)
    lines = ax.plot([],[])[0] # want to change the lines within that plot because live
    """
    Canvas
    """
    canvas = FigureCanvasTkAgg(fig, master = new_window)
    canvas.get_tk_widget().place(x = 10, y = 10, width = 500, height = 400) # create the canvas
    canvas.draw()
    """
    ------------------------------------------------------------------------------
                                     Create Buttons
    ------------------------------------------------------------------------------
    """
    new_window.update();
    start = tk.Button(new_window, text = "Start", font = ('calibri', 12), command = lambda: plot_start())
    start.place(x = 100, y = 425)
    
    new_window.update();
    stop = tk.Button(new_window, text = "Pause", font = ('calibri', 12), command = lambda: plot_stop())
    stop.place(x = start.winfo_x() +start.winfo_reqwidth() + 20, y = 425)
    
    new_window.update()
    end = tk.Button(new_window, text = "End", font = ('calibri', 12), command = lambda: plot_end())
    end.place(x = stop.winfo_x() + stop.winfo_reqwidth() + 20, y = 425)
    """
    ------------------------------------------------------------------------------
                                         Organization
    ------------------------------------------------------------------------------
    """
    # Organize where the laps are going to go
    # Organize where the Distance is going to go, Could create a button for meters
    # or feet whichever works                
    new_window.after(1, plot_data)
    new_window.update()


def user_data_button(): 
    global root
    """
    -------------------------------------------------------------------------------
                                    Hide Main Menu
    -----------------------------------------------------------------------------
    """
    root.withdraw()
    """
    ---------------------------------------------------------------------------
                              Open File Explorer
    ---------------------------------------------------------------------------
    """
    path = "C:/Users/rodri/Documents/School/Fall_2020/J/Project_Final/Data"
    path = os.path.realpath(path)
    os.startfile(path)
    os.chdir("C:/Users/rodri/Documents/School/Fall_2020/J/Project_Final")
    """
    ------------------------------------------------------------------------------
                                    GUI
    ------------------------------------------------------------------------------
    """
    # Create GUI with Image
    new_window_2 = tk.Toplevel(root)
    new_window_2.title("Previous Data")
    image_2 = Image.open("Images/image_5.jpg").resize((500,700), Image.ANTIALIAS)
    photo_2 = ImageTk.PhotoImage(image_2)
    canvas_image = tk.Canvas(new_window_2, width=500, height=700)
    canvas_image.pack()
    canvas_image.background = photo_2
    bg = canvas_image.create_image(0,0, anchor=tk.NW, image = photo_2)
    new_window_2.geometry("500x700")
    
    """
    --------------------------------------------------------------------------------
                                    Button Functionality
    --------------------------------------------------------------------------------
    """
    def submit_info():
        new_window_2.withdraw()
        user_input = clicked.get()#entry_1.get() + '.csv'
        
        """
        ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                     Get The Window
        +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        """
        
        new_window_3 = tk.Toplevel(root)
        new_window_3.title("Previous Data")
        image_3 = Image.open("Images/image_5.jpg").resize((500,700), Image.ANTIALIAS)
        photo_3 = ImageTk.PhotoImage(image_2)
        canvas_image = tk.Canvas(new_window_3, width=500, height=700)
        canvas_image.pack()
        canvas_image.background = photo_3
        bg = canvas_image.create_image(0,0, anchor=tk.NW, image = photo_3)
        new_window_3.geometry("500x700")
        
        """
        +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                        Display
        +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        """
        #os.chdir("C:/Users/rodri/Documents/School/Fall_2020/J/Project_Final/Data")
        os.chdir("C:/Users/rodri/Documents/School/Fall_2020/J/Project_Final/Data")
        path_file = user_input
        df = pd.read_csv(user_input)
        strokes = df['Strokes'][0]
        #calories = df['Calories'][0]
        time = df['Time'][0]
        distance = df['Distance'][0]
        
        label_a = Label(new_window_3, text = "Strokes Completed:  " + str(strokes), font = ('calibri', 20))
        #label_b = Label(new_window_3, text = "Calories Burnt:" + str(calories))
        label_c = Label(new_window_3, text = "Time Spent:  " + str(time), font = ('calibri', 20))
        label_d = Label(new_window_3, text = "Distance Rowed:  " + str(distance), font = ('calibri', 20))
        
        label_a.place(x = 20, y = 100)
        #label_b.place(x = 20, y = 200)
        label_c.place(x = 20, y = 200)
        label_d.place(x = 20, y = 300)
        
        """
        +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                     Button Functionality
        +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        """
        def submit_info_return():
            # Create GUI with Image
            new_window_2.deiconify()
            os.chdir("C:/Users/rodri/Documents/School/Fall_2020/J/Project_Final")
            new_window_3.destroy()
        """
        ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                     Button
        ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        """
        new_window_3.update();
        return_3 = tk.Button(new_window_3,bd = 6, text = "Return", font = ('calibri', 25), command = submit_info_return)
        return_3.place(x = 165, y = 520)
    
    def back_menu():
        root.deiconify()
        new_window_2.destroy()
        # change file directory
        os.chdir("C:/Users/rodri/Documents/School/Fall_2020/J/Project_Final")
    """
    ------------------------------------------------------------------------------
                                    Buttons
    ------------------------------------------------------------------------------
    """
    new_window_2.update();
    submit = tk.Button(new_window_2,bd = 6, text = "Submit", font = ('calibri', 25), command = submit_info)
    submit.place(x = 45, y = 520)
    
    new_window_2.update();
    back = tk.Button(new_window_2,bd = 6, text = "Return", font = ('calibri', 25), command = back_menu)
    back.place(x = submit.winfo_x() +submit.winfo_reqwidth() + 50, y = 520)

    """
    ------------------------------------------------------------------------------
                                     User Input
    -----------------------------------------------------------------------------
    """
    # Input User Information
   # entry_1 = Entry(new_window_2, bd = 6)
    #entry_1.place(x = 150, y = 50)
        # Get the list of files
    directory = 'C:\\Users\\rodri\\Documents\\School\\Fall_2020\\J\\Project_Final\\Data'
    list_files = os.listdir(directory)
    
    # Drop Down Menu
    clicked = StringVar()
    clicked.set("Select Your Workout!")
    drop = OptionMenu(new_window_2, clicked, *list_files)
    drop.pack()
    drop.place(x = 100, y = 100)

'''
===============================================================================
                               Create Main UI
===============================================================================
'''
root = tk.Tk()
root.title("MainMenu")
image = Image.open("Images/image_3.jpg").resize((600,900))
photo = ImageTk.PhotoImage(image)
image_label = Label(image=photo)
image_label.image = photo
image_label.pack()
root.geometry("500x750")

'''
==============================================================================
                            Create Buttons
==============================================================================
'''
root.update();
work_out = tk.Button(root,bd = 6,text = "        Work Out!        ", font = ('calibri', 25), command = create_workout_window)
work_out.place(x = 37, y = 200)

root.update();
user_data = tk.Button(root, bd = 6, text = "Previous Workouts", font = ('calibri', 25), command = user_data_button)
user_data.place(x = 37, y = 400)

# mainloop() block, update() does not
root.mainloop()

